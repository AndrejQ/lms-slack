package ru.hh.school.lms.persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.persistence.cache.GradeCacheDao;
import ru.hh.school.lms.persistence.cache.HomeworkCacheDao;
import ru.hh.school.lms.persistence.cache.LectureCacheDao;
import ru.hh.school.lms.persistence.cache.PersonCacheDao;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.persistence.entity.Person;

import java.time.LocalDate;

public class CacheLmsTest {
  CacheDaoManager cacheDaoManager;
  private GradeCacheDao gradeCacheDao;
  private HomeworkCacheDao homeworkCacheDao;
  private LectureCacheDao lectureCacheDao;
  private PersonCacheDao personCacheDao;

  @Before
  public void populateData() throws LmsValueNotFoundException {
    cacheDaoManager = new CacheDaoManager();
    gradeCacheDao = cacheDaoManager.getGradeTempDao();
    homeworkCacheDao = cacheDaoManager.getHwTempDao();
    lectureCacheDao = cacheDaoManager.getLectureTempDao();
    personCacheDao = cacheDaoManager.getPersonTempDao();

    Person lecturer1 = new Person("email@lec1", "Petya", false, true, false);
    Person lecturer2 = new Person("email@lec2", "Vasua", false, true, false);

    Person student1 = new Person("email@stud1", "Jeca", true, false, false);
    Person student2 = new Person("email@stud2", "Tolya121212", true, false, false);
    Person student3 = new Person("email@stud2", "Tolya", true, false, false);

    personCacheDao.savePerson(lecturer1);
    personCacheDao.savePerson(lecturer2);
    personCacheDao.savePerson(student1);
    personCacheDao.savePerson(student2);
    personCacheDao.savePerson(student3);

    String specialization = "All";
    boolean isHw = true;
    Lecture lecture1 = new Lecture("lec1", LocalDate.of(2019, 10, 10), lecturer1, specialization, isHw);
    Lecture lecture2 = new Lecture("lec2", LocalDate.of(2019, 9, 9), lecturer2, specialization, isHw);
    Lecture lecture3 = new Lecture("lec2", LocalDate.of(2019, 8, 8), lecturer2, specialization, isHw);

    lectureCacheDao.saveLecture(lecture1);
    lectureCacheDao.saveLecture(lecture2);
    lectureCacheDao.saveLecture(lecture3);

    // flush temp cache to main;
    cacheDaoManager.saveTemporaryCache();
    gradeCacheDao = cacheDaoManager.getGradeDao();
    homeworkCacheDao = cacheDaoManager.getHwDao();
    lectureCacheDao = cacheDaoManager.getLectureDao();
    personCacheDao = cacheDaoManager.getPersonDao();
  }

  @Test
  public void emptyTempCache() {
    Assert.assertTrue(cacheDaoManager.getPersonTempDao().getAllPersons().isEmpty());
    Assert.assertTrue(cacheDaoManager.getLectureTempDao().getLecturesSorted().isEmpty());
    Assert.assertTrue(cacheDaoManager.getHwTempDao().getHomeworksSortedByDeadline().isEmpty());
  }

  @Test
  public void homeworkTest() throws LmsValueNotFoundException {
    String lectureTitle = "lec1";
    Assert.assertTrue(homeworkCacheDao.getHomeworkByLectureTitle(lectureTitle).isEmpty());
    homeworkCacheDao.saveHomework(lectureTitle, LocalDate.now(), "Github.Link");
    Assert.assertTrue(homeworkCacheDao.getHomeworkByLectureTitle(lectureTitle).isPresent());
  }

  @Test
  public void pileOfOperationsTest() throws LmsValueNotFoundException {
    homeworkCacheDao.saveHomework(
      new Homework(lectureCacheDao.getLecturesSorted().get(1), LocalDate.now(), "github.link")
    );

    homeworkCacheDao.saveHomework(lectureCacheDao.getLecturesSorted().get(0).getTitle(), LocalDate.now(), "github.link2");

    gradeCacheDao.saveGrade(
      personCacheDao.getStudents().get(1).getEmail(),
      homeworkCacheDao.getHomeworksSortedByDeadline().get(1).getLecture().getTitle(),
      10
    );
    String student1Email = personCacheDao.getStudents().get(1).getEmail();
    String hwTitle = homeworkCacheDao.getHomeworksSortedByDeadline().get(1).getLecture().getTitle();
    Assert.assertTrue(gradeCacheDao.getGradeByStudentByHomework(student1Email, hwTitle).isPresent());
  }

  @Test(expected = LmsValueNotFoundException.class)
  public void wrongLectureTest() throws LmsValueNotFoundException {
    homeworkCacheDao.saveHomework("lecture not exist", LocalDate.now(), "sample github link");
  }

  @Test(expected = LmsValueNotFoundException.class)
  public void wrongHwTest() throws LmsValueNotFoundException {
    gradeCacheDao.saveGrade("hw not exist", "not exist", 1);
  }

  @Test(expected = LmsValueNotFoundException.class)
  public void wrongStudentTest() throws LmsValueNotFoundException {
    lectureCacheDao.saveLecture("new Lecture", LocalDate.now(), "email not exist", "All", false);
  }
}
