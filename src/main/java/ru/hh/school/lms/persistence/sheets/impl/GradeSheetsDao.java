package ru.hh.school.lms.persistence.sheets.impl;

import ru.hh.school.lms.persistence.GradeDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.cache.GradeCacheDao;
import ru.hh.school.lms.persistence.entity.Grade;
import ru.hh.school.lms.persistence.sheets.GoogleSheetsSynchronizer;

import java.util.List;
import java.util.Optional;

public class GradeSheetsDao implements GradeDao {
  private final GradeCacheDao gradeCacheDao;
  private final GoogleSheetsSynchronizer synchronizer;

  public GradeSheetsDao(GradeCacheDao gradeCacheDao, GoogleSheetsSynchronizer synchronizer) {
    this.gradeCacheDao = gradeCacheDao;
    this.synchronizer = synchronizer;
  }

  @Override
  public void saveGrade(String studentEmail, String lectureTitleOfHw, Integer gradeValue) throws LmsValueNotFoundException {
    gradeCacheDao.saveGrade(studentEmail, lectureTitleOfHw, gradeValue);
    synchronizer.cacheChanged();
  }

  @Override
  public List<Grade> getStudentGrades(String studentEmail) {
    return gradeCacheDao.getStudentGrades(studentEmail);
  }

  @Override
  public Optional<Grade> getGradeByStudentByHomework(String studentEmail, String hwLectureTitle) {
    return gradeCacheDao.getGradeByStudentByHomework(studentEmail, hwLectureTitle);
  }
}
