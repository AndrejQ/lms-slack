package ru.hh.school.lms.sheets;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesResponse;
import com.google.api.services.sheets.v4.model.BooleanCondition;
import com.google.api.services.sheets.v4.model.CellData;
import com.google.api.services.sheets.v4.model.CellFormat;
import com.google.api.services.sheets.v4.model.ConditionValue;
import com.google.api.services.sheets.v4.model.DataValidationRule;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.NumberFormat;
import com.google.api.services.sheets.v4.model.RepeatCellRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.SetDataValidationRequest;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SheetsClient {
  private final Sheets sheetsService;
  private final Map<String, Integer> sheetIdByTitleMap;
  private final String spreadsheetId;
  private String dateFormat;

  public SheetsClient(Sheets sheetsService, String spreadsheetId, String dateFormat) throws IOException {
    this.sheetsService = sheetsService;
    this.spreadsheetId = spreadsheetId;
    this.dateFormat = dateFormat;

    sheetIdByTitleMap = new HashMap<>();
    Spreadsheet spreadsheet = sheetsService.spreadsheets().get(spreadsheetId).execute();
    for (Sheet sheet : spreadsheet.getSheets()) {
      sheetIdByTitleMap.put(sheet.getProperties().getTitle(), sheet.getProperties().getSheetId());
    }
  }

  public Sheets getSheets() {
    return sheetsService;
  }

  public BatchUpdateValuesResponse batchUpdateValues(List<ValueRange> data) throws IOException {
    BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
      .setValueInputOption("USER_ENTERED")
      .setData(data);

    BatchUpdateValuesResponse batchResult = sheetsService.spreadsheets().values()
      .batchUpdate(spreadsheetId, batchBody)
      .execute();
    return batchResult;
  }

  public List<ValueRange> readValueRanges(List<String> rangesToRead) throws IOException {
    BatchGetValuesResponse readResult = getSheets().spreadsheets().values()
      .batchGet(spreadsheetId)
      .setRanges(rangesToRead)
      .execute();

    return readResult.getValueRanges();
  }

  public Integer getSheetIdByTitle(String sheetTitle) {
    return sheetIdByTitleMap.get(sheetTitle);
  }

  public BatchUpdateSpreadsheetResponse executeBatchUpdate(List<Request> requests) throws IOException {
    BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
    requestBody.setRequests(requests);
    return sheetsService.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
  }

  public Request dateRenderRequest(int columnIndex, int sheetId) {
    return new Request().setRepeatCell(
      new RepeatCellRequest()
        .setRange(getColumnGridRange(columnIndex, sheetId))
        .setCell(new CellData().setUserEnteredFormat(new CellFormat().setNumberFormat(
          new NumberFormat().setType("DATE").setPattern(dateFormat)
        )))
        .setFields("*")
    );
  }

  public Request dateValidationRequest(int columnIndex, int sheetId) {
    return dataValidationRequest(getColumnGridRange(columnIndex, sheetId), "DATE_IS_VALID", null);
  }

  public GridRange getColumnGridRange(int columnIndex, int sheetId) {
    int headerOffset = 1;
    return new GridRange().setSheetId(sheetId).setStartColumnIndex(columnIndex).setEndColumnIndex(columnIndex + 1).setStartRowIndex(headerOffset);
  }

  public Request dataValidationRequest(GridRange gridRange, String type, List<ConditionValue> conditionValues) {
    return new Request().setSetDataValidation(
      new SetDataValidationRequest()
        .setRange(gridRange)
        .setRule(new DataValidationRule().setCondition(
          new BooleanCondition().setType(type).setValues(conditionValues)
        ).setShowCustomUi(false).setStrict(true))
    );
  }
}
