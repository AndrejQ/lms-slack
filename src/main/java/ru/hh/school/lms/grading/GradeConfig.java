package ru.hh.school.lms.grading;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.persistence.GradeDao;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.PersonDao;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;

import java.util.Locale;

@Configuration
public class GradeConfig {

  @Bean(name = "grading")
  MessageManager message(Locale locale) {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("grading/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return (messageCode, args) -> messageSource.getMessage(messageCode, args, locale);
  }

  @Bean
  GradeFlow gradeFlow(
    SlackHelper slack,
    CallbackDispatcher dispatcher,
    @Qualifier("grading") MessageManager message,
    PersonDao personDao,
    HomeworkDao homeworkDao,
    GradeDao gradeDao
  ) {
    return new GradeFlow(slack, dispatcher, message, personDao, homeworkDao, gradeDao);
  }

}
