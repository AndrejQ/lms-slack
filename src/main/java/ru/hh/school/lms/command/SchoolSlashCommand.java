package ru.hh.school.lms.command;

import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.app_backend.interactive_messages.response.ActionResponse;
import ru.hh.school.lms.role.Role;
import ru.hh.school.lms.role.RoleResolver;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.SlashCommandEvent;
import ru.hh.school.lms.slack.event.SlashCommandHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

class SchoolSlashCommand implements SlashCommandHandler {

  private final SlackHelper slack;
  private final RoleResolver resolver;

  private final HeaderBlocks headerBlocks;
  private final StudentBlocks studentBlocks;
  private final LecturerBlocks lecturerBlocks;
  private final String command;

  SchoolSlashCommand(SlackHelper slack,
                     RoleResolver resolver,
                     Properties settings,
                     HeaderBlocks headerBlocks,
                     StudentBlocks studentBlocks,
                     LecturerBlocks lecturerBlocks) {
    this.slack = slack;
    this.resolver = resolver;
    this.command = settings.getProperty("ru.hh.school.lms.command.main");
    this.headerBlocks = headerBlocks;
    this.studentBlocks = studentBlocks;
    this.lecturerBlocks = lecturerBlocks;
  }

  @Override
  public void onEvent(SlashCommandEvent event) throws EventHandlingException {
    if (!command.equals(event.getCommand())) {
      return;
    }
    String userId = event.getUserId();
    Set<Role> roles = resolver.resolve(userId);
    List<LayoutBlock> blocks = new ArrayList<>();
    if (roles.contains(Role.LECTURER) || roles.contains(Role.ADMIN)) {
      blocks.addAll(lecturerBlocks.getBlocks());
    } else if (roles.contains(Role.STUDENT)) {
      blocks.addAll(studentBlocks.getBlocks(userId));
    }
    if (blocks.isEmpty()){
      blocks.addAll(headerBlocks.getBlocks());
    }
    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .blocks(blocks)
        .build()
    );
  }
}
