package ru.hh.school.lms.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.grading.GradeFlow;
import ru.hh.school.lms.homework.HomeworkFlow;
import ru.hh.school.lms.persistence.GradeDao;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.PersonDao;
import ru.hh.school.lms.role.RoleResolver;
import ru.hh.school.lms.sheets.SheetsClient;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Properties;
import java.util.function.Function;

@Configuration
class CommandConfig {

  @Autowired
  private Locale locale;

  @Bean
  StudentBlocks studentBlocks(SlackHelper slack, CallbackDispatcher dispatcher,
                              @Qualifier("studentMessage") MessageManager messageManager,
                              HeaderBlocks headerBlocks,
                              LectureDao lectureDao,
                              HomeworkDao homeworkDao,
                              GradeDao gradeDao,
                              PersonDao personDao,
                              @Qualifier("dayOfWeek") Function<LocalDate, String> dateFormatter,
                              @Qualifier("serviceProperties") Properties settings) {
    return new StudentBlocks(slack, dispatcher, messageManager, lectureDao, homeworkDao, gradeDao, personDao, headerBlocks, dateFormatter, settings);
  }

  @Bean
  LecturerBlocks lecturerBlocks(HomeworkFlow homeworkFlow,
                                GradeFlow gradeFlow,
                                CallbackDispatcher dispatcher,
                                @Qualifier("serviceProperties") Properties settings,
                                @Qualifier("lecturerMessage") MessageManager messageManager,
                                HeaderBlocks headerBlocks) {
    return new LecturerBlocks(homeworkFlow, gradeFlow, dispatcher, messageManager, settings, headerBlocks);

  }

  @Bean
  SchoolSlashCommand schoolSlashCommand(SlackHelper slack,
                                        RoleResolver resolver,
                                        HeaderBlocks headerBlocks,
                                        StudentBlocks studentBlocks,
                                        LecturerBlocks lecturerBlocks,
                                        @Qualifier("serviceProperties") Properties settings) {
    return new SchoolSlashCommand(slack, resolver, settings, headerBlocks, studentBlocks, lecturerBlocks);
  }

  @Bean
  HeaderBlocks headerBlocksBlocks(SheetsClient sheetsClient,
                                  LectureDao lectureDao,
                                  @Qualifier("serviceProperties") Properties settings,
                                  @Qualifier("commandMessage") MessageManager messageManager) {
    return new HeaderBlocks(messageManager, settings, sheetsClient, lectureDao);
  }

  @Bean(name = "commandMessage")
  public MessageManager commandMessage() {
    return messages("command");
  }

  @Bean(name = "lecturerMessage")
  public MessageManager lecturerMessage() {
    return messages("lecturer");
  }

  @Bean(name = "adminMessage")
  public MessageManager adminMessage() {
    return messages("admin");
  }

  @Bean(name = "studentMessage")
  public MessageManager studentMessage() {
    return messages("student");
  }

  private MessageManager messages(String path) {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames(path + "/labels", "sheets/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return (messageCode, args) -> messageSource.getMessage(messageCode, args, locale);
  }
}
