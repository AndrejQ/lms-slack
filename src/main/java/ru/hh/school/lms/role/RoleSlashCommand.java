package ru.hh.school.lms.role;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.SlashCommandEvent;
import ru.hh.school.lms.slack.event.SlashCommandHandler;

import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Component
final class RoleSlashCommand implements SlashCommandHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SlackHelper.class);
  private final String command;
  private RoleResolver roleResolver;
  private SlackHelper slack;
  private MessageManager message;

  @Autowired
  public RoleSlashCommand(RoleResolver roleResolver,
                          SlackHelper slack,
                          @Qualifier("serviceProperties") Properties settings,
                          @Qualifier("roleMessage") MessageManager message) {
    this.roleResolver = roleResolver;
    this.slack = slack;
    this.message = message;
    this.command = settings.getProperty("ru.hh.school.lms.command.role");
  }

  @Override
  public void onEvent(SlashCommandEvent event) throws EventHandlingException {
    if (!command.equals(event.getCommand())) {
      return;
    }
    String text = event.getText(); // text = <@UGHDF337U|username>
    String userId = event.getUserId();
    if (text != null && !text.isEmpty()) {
      int userIdStartIndex = text.indexOf('@') + 1;
      int userIdEndIndex = text.indexOf('|');
      userId = text.substring(userIdStartIndex, userIdEndIndex);
    }
    Set<Role> roles = roleResolver.resolve(userId);
    String user = "<@" + userId + ">";
    String msg = message.of("no_roles", user);
    if (!roles.isEmpty()) {
      String rolesEnumeration = roles.stream().map(Objects::toString).collect(Collectors.joining(","));
      msg = message.of("role_response", user, rolesEnumeration);
    }
    slack.postEphemeral(event.getChannelId(), event.getUserId(), msg, emptyList());
  }

}
