package ru.hh.school.lms.role;

import com.github.seratch.jslack.api.model.Conversation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

final class ChannelRoleResolver implements RoleResolver {

  private static final Logger LOGGER = LoggerFactory.getLogger(ChannelRoleResolver.class);


  private final SlackHelper slack;
  private final String adminChannelName;
  private final String lecturerChannelName;
  private final String studentChannelName;


  public ChannelRoleResolver(SlackHelper slack, Properties settings) {
    this.slack = slack;
    this.adminChannelName = settings.getProperty("ru.hh.school.lms.role.admin.channel");
    this.lecturerChannelName = settings.getProperty("ru.hh.school.lms.role.lecturer.channel");
    this.studentChannelName = settings.getProperty("ru.hh.school.lms.role.student.channel");
  }

  @Override
  public Set<Role> resolve(String userId) {
    return resolveWithSlack(userId);
  }

  private EnumSet<Role> resolveWithSlack(String userId) {
    EnumSet<Role> result = EnumSet.noneOf(Role.class);
    try {
      Set<Conversation> channels = slack.getPublicAndPrivateChannelsByUserId(userId);
      for (Conversation channel : channels) {
        if (channel.isArchived()) {
          continue;
        }
        Optional<Role> role = getRoleForChannel(channel.getName());
        role.ifPresent(result::add);
      }
    } catch (EventHandlingException exc) {
      LOGGER.warn("An error occurred during role resolving: ", exc);
    }
    return result;
  }

  private Optional<Role> getRoleForChannel(String channelName) {
    if (adminChannelName.equals(channelName)) {
      return Optional.of(Role.ADMIN);
    } else if (lecturerChannelName.equals(channelName)) {
      return Optional.of(Role.LECTURER);
    } else if (studentChannelName.equals(channelName)) {
      return Optional.of(Role.STUDENT);
    }
    return Optional.empty();
  }
}
