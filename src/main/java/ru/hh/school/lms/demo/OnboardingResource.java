package ru.hh.school.lms.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.hh.school.lms.onboarding.Onboarding;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/demo")
public final class OnboardingResource {
  private static final Logger LOGGER = LoggerFactory.getLogger(OnboardingResource.class);

  private final Onboarding onboarding;
  private final ApplicationContext thisContext;

  @Autowired
  public OnboardingResource(Onboarding onboarding,
                            ApplicationContext thisContext) {
    this.onboarding = onboarding;
    this.thisContext = thisContext;
  }

  @POST
  @Path("/onboarding")
  @Produces(MediaType.APPLICATION_JSON)
  public Response processSlashCommand() {
    onboarding.sendOnboarding(false);
    return Response.ok().build();
  }
}
