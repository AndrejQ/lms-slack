package ru.hh.school.lms.demo;

import javax.ws.rs.FormParam;

public class DemoEvent {
  @FormParam("channel")
  private String channelName;


  @FormParam("type")
  private String typeOfNotification;

  public DemoEvent() {
  }

  public String getChannelName() {
    return channelName;
  }

  public void setChannelName(String channelName) {
    this.channelName = channelName;
  }

  public String getTypeOfNotification() {
    return typeOfNotification;
  }

  public void setTypeOfNotification(String typeOfNotification) {
    this.typeOfNotification = typeOfNotification;
  }
}
