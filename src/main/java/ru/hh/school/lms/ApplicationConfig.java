package ru.hh.school.lms;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import ru.hh.nab.starter.NabProdConfig;
import ru.hh.school.lms.persistence.DataLmsConfig;
import ru.hh.school.lms.sheets.SheetsConfig;
import ru.hh.school.lms.slack.SlackConfig;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Properties;
import java.util.function.Function;

@Configuration
@Import({NabProdConfig.class, SlackConfig.class, DataLmsConfig.class, SheetsConfig.class})
@ComponentScan("ru.hh.school.lms")
class ApplicationConfig {

  @Bean
  Locale locale() {
    return new Locale("ru", "RU");
  }

  @Bean
  DateTimeFormatter dateTimeFormatter(@Qualifier("serviceProperties") Properties settings) {
    String dateFormat = settings.getProperty("sheets.date.format");
    return DateTimeFormatter.ofPattern(dateFormat);
  }

  @Bean
  @Primary
  Function<LocalDate, String> dateFormatter(DateTimeFormatter formatter) {
    return localDate -> localDate.format(formatter);
  }


  @Bean("dayOfWeek")
  Function<LocalDate, String> dayOfWeekDateFormatter(@Qualifier("serviceProperties") Properties settings) {
    String dateFormat = settings.getProperty("sheets.date.format");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat + " EEE", locale());
    return formatter::format;
  }


  @Bean
  Function<String, LocalDate> dateParser(DateTimeFormatter formatter) {
    return localDateString -> LocalDate.parse(localDateString, formatter);
  }
}
