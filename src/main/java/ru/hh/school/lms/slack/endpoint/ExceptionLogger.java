package ru.hh.school.lms.slack.endpoint;

import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionLogger implements ApplicationEventListener, RequestEventListener {

  public ExceptionLogger() {
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionLogger.class);

  @Override
  public void onEvent(final ApplicationEvent applicationEvent) {
  }

  @Override
  public RequestEventListener onRequest(final RequestEvent requestEvent) {
    return this;
  }

  @Override
  public void onEvent(RequestEvent paramRequestEvent) {
    if (paramRequestEvent.getType() == RequestEvent.Type.ON_EXCEPTION) {
      LOGGER.error("", paramRequestEvent.getException());
    }
  }
}
