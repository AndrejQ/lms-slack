package ru.hh.school.lms.slack.interactionexception;

public class ErrorItem {

  private final String name;
  private final String error;

  public ErrorItem(String name, String error) {
    this.name = name;
    this.error = error;
  }

  public String getName() {
    return name;
  }

  public String getError() {
    return error;
  }
}
