package ru.hh.school.lms.slack.security;

import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import java.util.Properties;

@Provider
public class SignatureFeature implements DynamicFeature {
  @Autowired
  private Properties serviceProperties;

  @Override
  public void configure(ResourceInfo resourceInfo, FeatureContext context) {
    if (resourceInfo.getResourceClass().isAnnotationPresent(Secured.class)) {
      context.register(new SignatureFilter(serviceProperties));
    }
  }
}
