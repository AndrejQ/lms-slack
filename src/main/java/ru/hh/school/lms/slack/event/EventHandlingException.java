package ru.hh.school.lms.slack.event;

public class EventHandlingException extends RuntimeException {

  private Event event;
  private String responseErrorMessage;

  public EventHandlingException(String message) {
    super(message);
  }

  public EventHandlingException(String message, Exception cause) {
    super(message, cause);
  }

  public EventHandlingException(String message, Event event) {
    super(message);
    this.event = event;
  }

  public EventHandlingException(String message, Exception cause, Event event) {
    super(message, cause);
    this.event = event;
  }

  public EventHandlingException(String message, String responseErrorMessage) {
    super(message + " Error message: " + responseErrorMessage);
    this.responseErrorMessage = responseErrorMessage;
  }

  public String getResponseErrorMessage() {
    return responseErrorMessage;
  }
}
