package ru.hh.school.lms.slack.blockaction;

public abstract class BlockAction {

  private String actionId;
  private String type;
  private String blockId;
  private String actionTs;

  public String getActionId() {
    return actionId;
  }

  public String getType() {
    return type;
  }

  public String getBlockId() {
    return blockId;
  }

  public String getActionTs() {
    return actionTs;
  }

  @Override
  public String toString() {
    return "BlockAction{" +
      "actionId='" + actionId + '\'' +
      ", type='" + type + '\'' +
      ", blockId='" + blockId + '\'' +
      ", actionTs='" + actionTs + '\'' +
      '}';
  }
}
