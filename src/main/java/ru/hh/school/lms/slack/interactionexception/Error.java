package ru.hh.school.lms.slack.interactionexception;

import java.util.List;

public class Error {
  private List<ErrorItem> errors;

  public List<ErrorItem> getErrors() {
    return errors;
  }

  public void setErrors(List<ErrorItem> errors) {
    this.errors = errors;
  }
}
