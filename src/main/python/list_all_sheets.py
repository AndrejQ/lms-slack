# pip install --upgrade google-api-python-client
# pip install oauth2client

import apiclient.discovery
import httplib2
from config import CREDENTIALS_FILE
from oauth2client.service_account import ServiceAccountCredentials

if __name__ == "__main__":
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE,
                                                                   ['https://www.googleapis.com/auth/spreadsheets',
                                                                    'https://www.googleapis.com/auth/drive'])
    httpAuth = credentials.authorize(httplib2.Http())
    drive_service = apiclient.discovery.build('drive', 'v3', http=httpAuth)
    files_list = drive_service.files().list().execute()
    print(files_list)
    for file in files_list['files']:
        if file['mimeType'] == 'application/vnd.google-apps.spreadsheet':
            print(file['id'] + " https://docs.google.com/spreadsheets/d/" + file['id'] + " " + file['name'])
